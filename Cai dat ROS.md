# README #

Readme này được tạo ra để hướng dẫn mọi người cài ROS trên ubuntu 16.4

Bước 1: cài VMware trên máy tính window

Bước 2: Tải ubuntu 16.04 theo link sau: https://drive.google.com/file/d/14jzy2HRtj-W_yeGlRfjAnzYQcz-Tp0Un/view?usp=sharing

Bước 3: Cài đặt ROS kinetic (tham khảo theo link sau: http://wiki.ros.org/kinetic/Installation/Ubuntu)
Đánh các lệnh sau vào cửa sổ termial trong ubuntu:

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
sudo apt-get update
sudo apt-get install ros-kinetic-desktop-full
sudo apt-get install ros-kinetic-PACKAGE hoặc sudo apt-get install ros-kinetic-slam-gmapping
apt-cache search ros-kinetic
sudo rosdep init
rosdep update
sudo apt-get install python-rosinstall python-rosinstall-generator python-wstool build-essential
